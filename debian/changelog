jsonhyperschema-codec (1.0.3-4) unstable; urgency=medium

  * Update standards version to 4.1.5, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 28 May 2022 00:52:13 +0100

jsonhyperschema-codec (1.0.3-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Tue, 03 May 2022 21:18:10 -0400

jsonhyperschema-codec (1.0.3-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 01:48:34 +0200

jsonhyperschema-codec (1.0.3-1) unstable; urgency=low

  * Initial release (Closes: #886786)

    This package provides a Python 2 version along with the Python3 version
    because all coreapi packages provide such Python2 version. It is thus a
    matter of consistency. As some packages depend on Python2 version of
    djangorestframework (which is the only thing that Depends on coreapi
    packages, I intend to commit myself at removing the python2 support for
    coreapi and djangorestframework as soon as buster is stable. I'll handle
    the transition as best as I can with the maintainers of the packages that
    transitively depend on coreapi.

 -- Pierre-Elliott Bécue <becue@crans.org>  Sat, 07 Apr 2018 12:25:32 +0200
